from dao.Conexion import *
from dominio.Entidades import *


class DaoPersona:
    def __init__(self):
        self.oc = Conexion()

    def insertar(self,tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "insert into persona(cedula,nombre,telefono,fecha_actualizacion) " \
              "values (%s,%s,%s,%s)"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")


    def actualizar(self,tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "UPDATE persona SET  nombre=%s,telefono=%s,fecha_actualizacion=%s" \
              " where cedula=%s"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")

    def getQuery(self, query):
        lista1 = []
        cone = self.oc.conecta()
        cursor = cone.cursor()
        cursor.execute(query)
        tupla1 = cursor.fetchall()
        for tupla in tupla1:
            obj = Persona(tupla[0],tupla[1], tupla[2],tupla[3], tupla[4])
            lista1.append(obj)
        cone.close()
        return lista1

