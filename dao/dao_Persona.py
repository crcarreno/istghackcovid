from dao.Conexion import *
from dominio.Entidades import *


class DaoPersona:
    def __init__(self):
        self.oc = Conexion()   #Objeto conexion

    def insertar(self, tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "insert into persona(cedula,nombre, telefono,fecha_actualizacion) " \
              "values (%s,%s,%s,%s)"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (cursor.lastrowid)

    def actualizar(self, tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "UPDATE persona SET  nombre=%s,telefono=%s,fecha_actualizacion=%s" \
              " where cedula=%s"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")

    def getQuery(self, query):
        lista1 = []
        cone = self.oc.conecta()
        cursor = cone.cursor()
        cursor.execute(query)
        tupla1 = cursor.fetchall()
        for tupla in tupla1:
            obj = Persona(tupla[0],tupla[1], tupla[2],tupla[3], tupla[4])
            lista1.append(obj)
        cone.close()
        return lista1
'''
crd = DaoPersona()
datos =("0922222222","MAYRA LOURDES","AYALA RODRIGUEZ","0890675555","2020-2-11")
#print(crd.insertar("db_covid",datos))
datos =("LINA MARINA","ROBLES ARCE","0890321234","2020-3-31","0922222222")
print(crd.actualizar("db_covid",datos))
sql = "select * from persona order by fecha_actualizacion"
lista = crd.getQuery("db_covid",sql)
for i in range(len(lista)):
    print(lista[i].cedula +" "+lista[i].nombre+" "+lista[i].apellido+" " +lista[i].telefono)
'''
