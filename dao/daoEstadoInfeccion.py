from dao.Conexion import *
from dominio.Entidades import *

class DaoEstadoInfeccion:
    def __init__(self):
        self.oc = Conexion()   #Objeto conexion

    def insertar(self, tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "insert into estado_infeccion(id,estado,descripcion,fecha_actualizacion) " \
              "values (%s,%s,%s,%s)"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")

    def actualizar(self,tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "UPDATE estado_infeccion SET estado=%s,descripcion=%s,fecha_actualizacion=%s" \
              " where id=%s"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")

    def getQuery(self, query):
        lista1 = []
        cone = self.oc.conecta()
        cursor = cone.cursor()
        cursor.execute(query)
        tupla1 = cursor.fetchall()
        for tupla in tupla1:
            obj = EstadoInfeccion(tupla[0], tupla[1], tupla[2], tupla[3])
            lista1.append(obj)
        cone.close()
        return lista1


