from dao.Conexion import *

class QueryJoins:
    def __init__(self):
        self.oc = Conexion()

    def getAllJoinTables(self):
        sql = "select p.cedula,p.id,p.nombre,p.telefono, e.estado, e.descripcion,r.latitud,r.longitud " + \
              "from persona p,estado_infeccion e, registro_persona_estado r " + \
              "where p.id=r.id_persona and e.id=r.id_estado"
        cone =self.oc.conecta()
        cursor = cone.cursor()
        cursor.execute(sql)
        tupla = cursor.fetchall()
        cone.close()
        return tupla

    def getCedulaJoinTables(self,cedula):
        sql = "select p.cedula,p.id,p.nombre,p.telefono, e.estado, e.descripcion,r.latitud,r.longitud " + \
              "from persona p,estado_infeccion e, registro_persona_estado r " + \
              "where p.id=r.id_persona and e.id=r.id_estado and cedula=%s"
        filtro=(cedula,)
        cone =self.oc.conecta()
        cursor = cone.cursor()
        cursor.execute(sql,filtro)
        tupla = cursor.fetchall()
        cone.close()
        return tupla

ob = QueryJoins()
