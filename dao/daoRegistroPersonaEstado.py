from dao.Conexion import *
from dominio.Entidades import *
import logging

logger = logging.getLogger(__name__)

class DaoRegistroPersonaEstado:
    def __init__(self):
        self.oc = Conexion()   #Objeto conexion

    def insertar(self, tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "insert into registro_persona_estado(id_persona,id_estado, latitud," \
              "longitud,fecha_actualizacion) values (%s,%s,%s,%s,%s)"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()

        return (str(cursor.rowcount) + " registro(s) afectados")

    def actualizar(self,tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "UPDATE registro_persona_estado SET id_persona=%s, id_estado=%s,latitud=%s," \
              "longitud=%s, fecha_actualizacion=%s  where id=%s"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")

    def getQuery(self, query):
        lista1 = []
        cone = self.oc.conecta()
        cursor = cone.cursor()
        cursor.execute(query)
        tupla1 = cursor.fetchall()
        for tupla in tupla1:
            obj = RegistroPersonaEstado(tupla[0],tupla[1], tupla[2],tupla[3], tupla[4],tupla[5])
            lista1.append(obj)
        cone.close()
        return lista1

    def getUbications(self):
        con = self.oc.conecta()
        cur = con.cursor()
        cur.execute('SELECT id, id_estado, latitud, longitud FROM registro_persona_estado')

        list=[]

        for data in cur.fetchall():
            #obj = (data[0], data[1], data[2], data[3])
            list.append(data)
        #dict = {key: val for key, val in cur.fetchall()}

        cur.close()
        con.close()

        #logger.warning(dict)

        return list

'''
crd = DaoRegistroPersonaEstado()
datos =(1,0,30,20,"2020-2-10","leve")
#print(crd.insertar("db_covid",datos))
datos =(3,1,30,20,"2020-2-10","grave",2)
print(crd.actualizar("db_covid",datos))

sql = "select * from registro_persona_estado order by fecha_actualizacion"
lista = crd.getQuery("db_covid",sql)
for i in range(len(lista)):
    print(str(lista[i].id_registro)+" "+str(lista[i].id_persona)+" "+str(lista[i].id_estado)+" "+str(lista[i].latitud))
'''

