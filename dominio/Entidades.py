class Persona:

    def __init__(self,id_persona,cedula,nombre,telefono,fecha_actualizacion):
        self.id_persona =id_persona
        self.cedula = cedula
        self.nombre = nombre
        self.telefono=telefono
        self.fecha_actualizacion=fecha_actualizacion


class RegistroPersonaEstado:

    def __init__(self,id_registro,id_persona, id_estado,latitud,longitud, fecha_actualizacion):
        self.id_registro = id_registro
        self.id_persona = id_persona
        self.id_estado = id_estado
        self.latitud = latitud
        self.longitud = longitud
        self.fecha_actualizacion=fecha_actualizacion

