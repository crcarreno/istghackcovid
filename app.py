# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, make_response
import logging
#from dao import dao_Persona, daoRegistroPersonaEstado

from models import persona, institutos
logger = logging.getLogger(__name__)
app = Flask(__name__)


#@app.route('/')
#def index():
#    return render_template('index.html')

@app.route('/', methods=['GET', 'POST'])
def index():

    #regPerEst = daoRegistroPersonaEstado.DaoRegistroPersonaEstado()

    profesores = persona.Persona().getUbications()
    unidades = institutos.Instituto().getUbications()

    return render_template('index.html', profesores=profesores, unidades=unidades)

    '''if request.method == 'POST':
        if request.form['cedula'] and request.form['telefono'] and request.form['nombres'] and request.form['latitud'] and request.form['longitud']:
            in_cedula = request.form['cedula']
            in_telef = request.form['telefono']
            in_nombres = request.form['nombres']
            in_lat = request.form['latitud']
            in_lgn = request.form['longitud']

            persona = dao_Persona.DaoPersona()

            datos_persona = (in_cedula, in_nombres, in_telef, "2020-2-11")

            datos_regPerEst = (persona.insertar(datos_persona), 0, in_lat, in_lgn, "2020-2-10")

            regPerEst.insertar(datos_regPerEst)

            #if regPerEst.insertar(datos_regPerEst) > 0:
            #    error = 1

            #resp = make_response(json.dumps(data))
            #resp.status_code = 200
            #resp.headers['Access-Control-Allow-Origin'] = '*'

            #logger.warning(data)

            #data = regPerEst.getUbications()

    colors = ['#ff0000', '#0000ff', '#ffffe0', '#008000', '#800080', '#FFA500']

    return render_template('index.html', validar_data=error, colors=colors)'''


@app.route('/form_person', methods=['GET', 'POST'])
def form_person():
    return render_template('form_person.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
