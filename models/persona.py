from models.conexion import *
import logging

logger = logging.getLogger(__name__)


class Persona:
    def __init__(self):
        self.oc = Conexion()

    def insertar(self,tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "insert into persona(cedula,nombres,mail,telefono,fecha_act,latitud,longitud) " \
              "values (%s,%s,%s,%s,%s,%s,%s)"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")

    def actualizar(self,tupla):
        cone = self.oc.conecta()
        cursor = cone.cursor()
        sql = "UPDATE persona SET  nombre=%s,telefono=%s,fecha_actualizacion=%s" \
              " where cedula=%s"
        cursor.execute(sql, tupla)
        cone.commit()
        cone.close()
        return (str(cursor.rowcount) + " registro(s) afectados")

    def getUbications(self):
        con = self.oc.conecta()
        cur = con.cursor()
        cur.execute('SELECT latitud, longitud, estado, nombres, telefono FROM hm_persona')

        list=[]

        for data in cur.fetchall():
            list.append(data)

        cur.close()
        con.close()

        #logger.warning(dict)

        return list
