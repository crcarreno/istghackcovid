from models.conexion import *
import logging

logger = logging.getLogger(__name__)


class Instituto:
    def __init__(self):
        self.oc = Conexion()

    def getUbications(self):
        con = self.oc.conecta()
        cur = con.cursor()
        cur.execute('''SELECT nombre, direccion, cant_docentes, latitud, longitud,
                     hm_instituto_sostenimiento.tipo, hm_instituto_zona.tipo
                    FROM hm_instituto
                    INNER JOIN hm_instituto_sostenimiento ON hm_instituto.id_sostenimiento = hm_instituto_sostenimiento.id
                    INNER JOIN hm_instituto_zona ON hm_instituto.id_zona = hm_instituto_zona.id''')

        list=[]

        for data in cur.fetchall():
            list.append(data)

        cur.close()
        con.close()

        #logger.warning(dict)

        return list